package com.isi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/message")
@SpringBootApplication
public class GitlabCicdApplication {
	@GetMapping
	public ResponseEntity<String> message(){
		return ResponseEntity.ok("Gitlab CICD");
	}
	public static void main(String[] args) {
		SpringApplication.run(GitlabCicdApplication.class, args);
	}

}
