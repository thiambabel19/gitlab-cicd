Pour réaliser ce travail, nous devons suivre ces différentes étapes :

1. Créer un référentiel GitLab
2. Configurer Docker Hub
3. Configurer Maven:

![img1](/uploads/9c3ab2ff2dcbeb77d270c18ccbe32b19/img1.png)

![img2](/uploads/a7bd99bd46d6a9d5e7f6d389bccde5c8/img2.png)

4. Configurer les étapes du pipeline : Dans le fichier '.gitlab-ci.yml':

![img3](/uploads/0694c5cc30fa8ce86b6748f575563f97/img3.png)

5. Exécuter le pipeline CI/CD:

![Capture_d_écran_2024-02-17_213825](/uploads/4f744f13148ee9c09d8c18e41e50a94e/Capture_d_écran_2024-02-17_213825.png)

![Capture_d_écran_2024-02-17_220930](/uploads/84d1e5d7283a8aa9988ac0159921b817/Capture_d_écran_2024-02-17_220930.png)

6. Vérifier le déploiement sur Docker Hub:

![Capture_d_écran_2024-02-17_220840](/uploads/6984f4b0e09c938c2186eb51c1d900d2/Capture_d_écran_2024-02-17_220840.png)
