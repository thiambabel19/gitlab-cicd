FROM openjdk:17-jdk-slim

LABEL maintainer = "Mohamed THIAM mohamedthiam563@groupeisi.com"

ADD target/gitlab-cicd.jar  gitlab-cicd.jar

CMD ["java", "-jar", "gitlab-cicd.jar"]